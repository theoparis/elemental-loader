package com.theoparis.elementalbridge

import com.theoparis.elementalbridge.mappings.MappingsLoader

object CoreLoader {
    fun isBootstrapped(): Boolean {
        val bootstrapClass = Class.forName(MappingsLoader.getClassDotted("net.minecraft.Bootstrap"))
        val initField =
            bootstrapClass.getDeclaredField(MappingsLoader.getField("initialized", "net.minecraft.Bootstrap"))
        initField.trySetAccessible()
        return initField.get(null) as Boolean
    }
}
