package com.theoparis.elementalbridge.mappings

import com.google.gson.GsonBuilder
import java.io.File

class MappingsGenerator {
    fun parseMapping(lines: List<String>): MappingsList {
        val mapping = MappingsList(mutableListOf(), mutableListOf(), mutableListOf())

        lines.slice(1 until lines.size).map {
            it.trimStart()
        }.forEach {
            if (it.startsWith("CLASS")) {
                val mappedName = it.split(Regex("\\s"))[1]
                val className = it.split(Regex("\\s"))[3]
                val clsMapping = ClassMapping(className, mappedName)
                mapping.classes.add(clsMapping)
            } else if (it.startsWith("FIELD")) {
                val fieldName = it.split(Regex("\\s"))[5]
                val mappedName = it.split(Regex("\\s"))[3]
                val className = it.split(Regex("\\s"))[1]
                val fieldType = it.split(Regex("\\s"))[2].replace("L", "").replace(";", "")
                val field = FieldMapping(className, fieldName, mappedName, fieldType)
                mapping.fields.add(field)
            } else if (it.startsWith("METHOD")) {
                val methodName = it.split(Regex("\\s")).getOrElse(5) { "" }
                val methodSignature = it.split(Regex("\\s")).getOrElse(2) { "" }
                val className = it.split(Regex("\\s")).getOrElse(1) { "" }
                val mappedName = it.split(Regex("\\s")).getOrElse(3) { "" }
//                val methodSignature = if (methodName.trim() === "<init>") it.split(" ")[3] else it.split(" ")[2]
                val method = MethodMapping(className, methodName, mappedName, methodSignature)
                mapping.methods.add(method)
            }
        }

        return mapping
    }

    fun generate(): MappingsList {
        return parseMapping(File("mappings.txt").readLines())
    }

    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            val mappings = MappingsGenerator().generate()
            val mappingsJson = GsonBuilder().setPrettyPrinting().disableHtmlEscaping().create().toJson(
                mappings
            )
            File(
                args.getOrElse(0) {
                    "mappings.json"
                }
            ).writeText(mappingsJson)
        }
    }
}
