# Elemental Mod Loader

A mod loader for Minecraft.

## Introduction

Elemental Mod Loader aims to provide an easy-to-use modding api
for Minecraft. You can write mods in Javascript or Typescript,
and bundle them with something like [esbuild](https://esbuild.github.io).

### How It Works

Behind the scenes, this mod loader is unlike any other mod loader.
The Elemental Loader injects code into Minecraft at runtime using ByteBuddy.

## Development

### Prerequisites

- Java 16
- Gradle
- Maven
- Kotlin

### Getting the Mappings

You can get the mappings either by compiling [yarn](https://github.com/FabricMC/yarn/tree/1.17.1) with
their `gradle build` script.

You can then put these mappings into mappings.txt in the root of the repository. To generate the json files that are
compatible with the Elemental API, simply run
`gradle genMappings` in the repository folder.

### Building the Agent

Running `gradle build` in the repository folder will generate the Java Agent that you can then use inside the JVM
Arguments of your minecraft launcher profile. Here's an example:

```
-javaagent:/home/theo/elementalmodding/build/libs/elemental-bridge-1.0.0.jar
```
