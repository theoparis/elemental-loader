/// <reference path="index.d.ts" />

export interface IItem {
  setId(id: string): void;
  getId(): string;

  new (id: string): IItem;
}

export interface IRegistry<T> {
  register(obj: T): void;
  new (modId: string, type: string): IRegistry<T>;
}

export interface IModData {
  id: string;
}

export interface IItemData {
  id: string;
}

export class Mod {
  readonly metadata: IModData;

  constructor(metadata: IModData) {
    this.metadata = metadata;
  }

  createRegistry<T>(registryType: string) {
    return new (Java.type<IRegistry<T>>(
      "com.theoparis.elementalbridge.Registry"
    ))(this.metadata.id, registryType);
  }

  createItem(data: IItemData) {
    return new (Java.type<IItem>("com.theoparis.elementalbridge.core.Item"))(
      data.id
    );
  }
}
